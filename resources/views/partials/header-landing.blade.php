<header class="header">
    <div class="container">
        <a class="header__logo">
            <img src="@asset('images/embla_robotics_header_logo.svg')"/>
        </a>

        <nav class="header__nav header__nav--landing">
            <ul>
                <li>
                    <a href="#cta" class="hash-scroll">Get a Free Quote</a>
                </li>
            </ul>
        </nav>
    </div>
</header>
