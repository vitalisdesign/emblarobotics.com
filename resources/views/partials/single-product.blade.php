<section class="products-single">
    <div class="container">
        <h1 class="products-single__heading">
            <?= str_replace('Automower', 'Husqvarna Automower<sup>&reg;</sup>', get_the_title()); ?>
        </h1>

        <div class="products-single__overview">
            <div class="products-single__image">
                <?php the_post_thumbnail(); ?>
            </div>

            <div class="products-single__details">
                <div class="products-single__description"><?php the_field('description'); ?></div>

                @include('partials.single-product-details')

                <div class="products-single__link">
                    <div class="product__price">
                        <div class="product__price__heading">Starting at</div>
                        <div class="product__price__value"><?= '$' . number_format(get_field('price'), 2, '.', ','); ?></div>
                    </div>

                    <?php
                    if (get_field('link')) { 
                        ?>
                        <a href="<?php the_field('link'); ?>" target="_blank" class="ui-button ui-button--tertiary">More info on Husqvarna.com</a>
                        <?php
                    }
                    ?>
                </div>

                <div class="product__financing">
                    <div>We offer up to 48 months interest free financing (via Sheffield and Synchrony)</div>
                </div>
            </div>
        </div>
    </div>

    <div class="products-single__cta">
        <h3 class="products-single__cta__heading">Get a free quote</h3>
        
        <div class="products-single__cta__text">Take the next step to never having to mow the lawn again. Click below to get your free, no obligation quote.</div>

        <a href="<?= get_permalink(get_page_by_path('free-quote')); ?>" class="ui-button ui-button--secondary">Get a Free Quote</a>
    </div>
</section>
