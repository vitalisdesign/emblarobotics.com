<section class="products-list">
    <div class="container">
        <?php
        $loop = new WP_Query([
            'post_type' => 'product',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page' => -1
        ]);
        while ($loop->have_posts()) : $loop->the_post();
            ?>
            <div class="products-list__item">
                <a href="<?php the_permalink(); ?>">
                    <h3 class="products-list__heading">
                        <?= str_replace('Automower', 'Automower<sup>&reg;</sup>', get_the_title()); ?>
                    </h3>

                    <div class="products-list__image">
                        <?php the_post_thumbnail('medium_large'); ?>
                    </div>
                </a>

                @include('partials.single-product-details')

                <a href="<?php the_permalink(); ?>" class="product__price">
                    <div class="product__price__heading">Starting at</div>
                    <div class="product__price__value"><?= '$' . number_format(get_field('price'), 2, '.', ','); ?></div>
                </a>
            </div>
            <?php
        endwhile;
        ?>
    </div>

    <div class="product__financing">
        <div>We offer up to 48 months interest free financing (via Sheffield and Synchrony)</div>
    </div>
</section>

<section class="downloads">
    <h2 class="downloads__heading">Downloads</h2>

    <div class="container">
        <?php
        $loop = new WP_Query([
            'post_type' => 'download',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page' => -1
        ]);
        while ($loop->have_posts()) : $loop->the_post();
            $file = get_field('file');
            ?>
            <a href="<?= $file['url']; ?>" class="downloads__item" target="_blank">
                <div class="downloads__item__icon"><i class="material-icons">file_download</i></div>

                <div class="downloads__item__image">
                    <?php the_post_thumbnail(); ?>
                </div>

                <h3 class="downloads__item__heading"><?php the_title(); ?></h3>
            </a>
            <?php
        endwhile;
        ?>
    </div>
</section>
