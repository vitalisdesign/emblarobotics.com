<header class="header">
    <?php $header_banner_enabled = get_field('header_banner_enabled', 'option'); ?>
    <?php if ($header_banner_enabled) { ?>
        <div class="header__banner">
            <span class="header__banner__heading header__banner__item"><?= get_field('header_banner_heading', 'option'); ?></span>
            <a href="<?= get_field('header_banner_link_url', 'option'); ?>" class="header__banner__item ui-button ui-button--secondary" target="_blank"><?= get_field('header_banner_link_text', 'option'); ?></a>
        </div>
    <?php } ?>

    <div class="container">
        <a class="header__logo" href="<?= esc_url(home_url('/')); ?>">
            <img src="@asset('images/embla_robotics_header_logo.svg')"/>
        </a>

        <div></div>

        <a class="header__nav__toggle">
            <i class="material-icons">menu</i>
        </a>

        <nav class="header__nav">
            <?php
            if (has_nav_menu('primary_navigation')) {
                wp_nav_menu([
                    'theme_location' => 'primary_navigation',
                    'container' => ''
                ]);
            }
            ?>
        </nav>
    </div>
</header>

<div class="header-spacer <?php echo $header_banner_enabled ? 'has-header-banner' : ''; ?>"><span></span></div>
