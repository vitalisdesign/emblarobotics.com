<footer class="page-footer">
    <div class="container">
        <p>&copy; <?php echo date('Y') . ' Embla Robotics, LLC. All Rights Reserved.'; ?></p>

        <p>Website Created by <a href="https://www.vitalisdesign.com" target="_blank" title="Website Created by Vitalis Design">Vitalis Design</a>.</p>
    </div>
</footer>
