<form class="free-quote__form">
    <div class="user-feedback"></div>

    <div class="free-quote__form__fields">
        <fieldset>
            <label for="first-name" class="sr-only">First name</label>
            <input type="text" name="first_name" id="first-name" placeholder="Your first name">
        </fieldset>
        <fieldset>
            <label for="email" class="sr-only">Email address</label>
            <input type="email" name="email" id="email" placeholder="Your email address">
        </fieldset>

        <button
            type="submit"
            class="ui-button ui-button--primary">
            Get a Free Quote
        </button>
    </div>
</form>
