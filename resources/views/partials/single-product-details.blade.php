<div class="product__spec">
    <div class="product__spec__name">
        <i class="material-icons">aspect_ratio</i>
        <span>Area capacity</span>
    </div>
    <div class="product__spec__value"><?= get_field('area_capacity') . ' acre'; ?></div>
</div>

<div class="product__spec">
    <div class="product__spec__name">
        <i class="material-icons">battery_charging_full</i>
        <span>Charge time</span>
    </div>
    <div class="product__spec__value"><?= get_field('charge_time') . ' min'; ?></div>
</div>

<div class="product__spec">
    <div class="product__spec__name">
        <i class="material-icons">timer</i>
        <span>Mow time</span>
    </div>
    <div class="product__spec__value"><?= get_field('mow_time') . ' min'; ?></div>
</div>

<div class="product__spec">
    <div class="product__spec__name">
        <i class="material-icons">sort</i>
        <span>Cutting height</span>
    </div>
    <div class="product__spec__value"><?= get_field('cutting_height'); ?></div>
</div>

<div class="product__features">
    <?php
    global $post;
    $features = get_field('features');
    $index = 1;
    foreach ($features as $feature) {
        ?>
        <div id="<?= $post->ID . '-' . $index; ?>" class="product__features__item">
            <img src="@asset('images/icons/features/' . $feature . '.svg')" />
            <div class="mdl-tooltip" for="<?= $post->ID . '-' . $index; ?>"><?= $feature; ?></div>
        </div>
        <?php
        $index++;
    }
    ?>
</div>
