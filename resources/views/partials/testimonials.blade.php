<?php
namespace App;
use WP_Query;
?>

<section id="testimonials" class="testimonials">
    <h3 class="testimonials__heading">Automower<sup>&reg;</sup> Success Stories</h3>
    <div class="testimonials__subheading">Click on the play button below to watch video testimonials</div>

    <div class="testimonials__slider">
        <?php
        $query = new WP_Query([
            'post_type' => 'testimonial',
            'posts_per_page' => -1,
            'orderby' => 'rand'
        ]);

        $index = 0;
        while ($query->have_posts()) : $query->the_post();
            ?>
            <div id="slide-<?= $index; ?>" class="testimonials__slide">
                <div class="testimonials__slide__filter">
                    <div class="testimonials__slide__quote">"<?= get_field('quote'); ?>"</div>

                    <div class="testimonials__slide__name"><?= get_field('name'); ?>, <?= get_field('city'); ?></div>

                    <i data-rel="<?= $index; ?>" class="testimonials__slide__play material-icons">play_circle_filled</i>
                </div>

                <div class="testimonials__slide__image" style="background-image: url(<?= get_field('background_image')['url']; ?>);"></div>

                <?php
                $youtube_video = parse_video_uri(get_field('youtube_video', false, false));
                ?>
                <div class="testimonials__slide__video">
                    <div class="video-wrapper">
                        <iframe id="player-<?= $index; ?>" width="1244" height="700" src="https://www.youtube.com/embed/<?= $youtube_video['id']; ?>?enablejsapi=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <?php
            $index++;
        endwhile;
        ?>
    </div>

    <div class="testimonials__pager">
        <?php
        $index = 0;
        while ($query->have_posts()) : $query->the_post();
            ?>
            <div class="testimonials__pager__item <?= $index == 0 ? 'active' : ''; ?>" data-rel="<?= $index; ?>">
                <img src="<?= get_field('headshot')['sizes']['medium']; ?>" />

                <div class="testimonials__pager__item__icon">
                    <i class="material-icons">play_arrow</i>
                </div>
            </div>
            <?php
            $index++;
        endwhile;
        ?>
    </div>
</section>

<script>
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var players = [];
    function onYouTubeIframeAPIReady() {
        jQuery('.testimonials__slide__video').each(function(index) {
            var player = new YT.Player('player-' + index, {
                events: {
                    'onStateChange': onPlayerStateChange
                }
            });
            players[index] = player;
        });
    }

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
            jQuery(event.target.a).parent().parent().parent().removeClass('playing');
            jQuery('.testimonials').removeClass('active');
        }
    }
</script>
