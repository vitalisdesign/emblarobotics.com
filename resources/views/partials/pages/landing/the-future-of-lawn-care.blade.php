<section class="home__intro home__section">
    <div class="home__intro__video-placeholder"
        style="background-image: url(@asset('images/intro_video_placeholder.jpg'));">
    </div>

    <div class="home__intro__video-container">
        <video autoplay="autoplay" loop="loop" muted="muted" poster="@asset('images/intro_video_poster.jpg')" class="home__intro__video">
            <source src="@asset('images/videos/landing_page_video.mp4')" type="video/mp4"/>
            <source src="@asset('images/videos/landing_page_video.webm')" type="video/webm"/>
        </video>
    </div>

    <div class="home__intro__panel-container container">
        <div class="home__intro__panel">
            <h2 class="home__intro__panel__heading">Get a better looking lawn<br /> with less effort</h2>
            <h3 class="home__intro__panel__subheading">The Husqvarna Automower<sup>&reg;</sup> is a robotic lawn mower that mows your lawn automatically, freeing you up to do the things you’d rather do.</h3>

            <div class="home__intro__panel__buttons">
                <a href="#testimonials" class="hash-scroll ui-button ui-button--default">Watch Video Testimonials</a>

                <a href="#cta" class="hash-scroll ui-button ui-button--primary">Get a Free Quote</a>
            </div>
        </div>
    </div>

    <a href="#intro" class="home__intro__arrow hash-scroll">
        <span>Learn More</span>
        <i class="material-icons">keyboard_arrow_down</i>
    </a>
</section>

<section id="intro" class="home__problem-solution home__section"
    style="background-image: url(@asset('images/home/daniel-watson-75022.jpg'));">

    <div class="home__problem-solution__content">
        <div>
            <h3 class="home__problem-solution__heading">No more mowing required</h3>

            <p>How much time do you spend mowing the lawn each week?</p>

            <p>What if you could spend that time doing sometime you actually enjoy?</p>

            <p>No more battling with the lawn mower to start, no more gasoline smell stinking up the garage. Not to mention all the space saved...</p>
        </div>

        <div>
            <h3 class="home__problem-solution__heading">Think the Roomba, but for your lawn</h3>

            <p>The Husqvarna Automower is like the Roomba, but for your lawn. Once installed, mowing will become a thing of the past.</p>

            <p>Just imagine a yard that looks great every day of the week. All without you having to lift a finger.</p>
        </div>
</section>

<section class="home__automower-intro home__section"
    style="background-image: url(@asset('images/grass_bg.jpg'));">
    <div class="container">
        <h1 class="home__automower-intro__heading">Husqvarna Automower<sup>&reg;</sup></h1>
        <h2 class="home__automower-intro__subheading">The Future of Lawn Care</h2>

        <div class="home__automower-intro__video">
            <div class="video-wrapper">
                <iframe width="1280" height="720" src="https://www.youtube.com/embed/UOoVv-jp7to?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>

            <a class="how-automower-works__intro__video__link" href="https://www.youtube.com/watch?v=KE7O3dK07nQ&index=2&list=PL0VcazyXHqEqXo1HAFT-I4UHXIf9WjO4R" target="_blank">
                <i class="material-icons">ondemand_video</i>
                <span>Watch more Automower<sup>&reg;</sup> videos</span>
            </a>
        </div>
    </div>
</section>

<section class="home__set-it-and-forget-it home__section fullwidth-bg-image"
    style="background-image: url(@asset('images/silence_is_golden.jpg'));">
    <div class="home__set-it-and-forget-it__panel-container container">
        <div class="home__set-it-and-forget-it__panel">
            <h3 class="home__panel-subheading">Set it and forget it</h3>
            <div class="home__panel-paragraph">Automower<sup>&reg;</sup> will keep your lawn looking great with little to no effort on your part. We’ll even handle the installation and maintenance.</div>

            <br /><br />

            <h3 class="home__panel-subheading">Silence is golden</h3>
            <div class="home__panel-paragraph">Unlike traditional lawn mowers, Automower<sup>&reg;</sup> operates almost silently. It can even mow while you sleep.</div>
        </div>
    </div>
</section>

<section class="home__a-better-looking-lawn home__section fullwidth-bg-image"
    style="background-image: url(@asset('images/state_of_the_art_technology.jpg'));">
    <div class="home__a-better-looking-lawn__panel-container container">
        <div class="home__a-better-looking-lawn__panel">
            <h3 class="home__panel-subheading">A better looking lawn</h3>
            <div class="home__panel-paragraph">A great looking lawn can be a lot of work. With Automower<sup>&reg;</sup>, your lawn is mowed automatically throughout the day, and the cuttings are small enough to add fertilization back to the soil.</div>

            <br /><br />

            <h3 class="home__panel-subheading">State-of-the-art technology</h3>
            <div class="home__panel-paragraph">Husqvarna has spent over 20 years developing the most advanced robotic lawn mower available. With GPS tracking and smart sensors, Husqvarna’s automowers are designed to easily maneuver over tricky terrain with slopes up to 45%.</div>
        </div>
    </div>
</section>

@include('partials.testimonials')

<section id="cta" class="home__cta home__section fullwidth-bg-image"
    style="background-image: url(@asset('images/the_future_of_lawn_care.jpg'));">
    <div class="home__cta__panel-container container">
        <div class="home__cta__panel">
            <h2 class="home__cta__panel__heading">Get a free quote</h2>

            <p class="home__cta__panel__copy">Take the next step to never having to mow the lawn again. Enter your first name and email address below to get your free, no obligation quote.</p>

            @include('partials.forms.free-quote')
        </div>
    </div>
</section>
