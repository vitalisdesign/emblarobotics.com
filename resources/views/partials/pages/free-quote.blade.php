<section class="free-quote__atf">
    <div class="free-quote__atf__image"
        style="background-image: url(@asset('images/becky.jpg'));">
    </div>

    <div class="free-quote__atf__panel-container container">
        <div class="free-quote__atf__panel">
            <h2 class="free-quote__atf__panel__heading">Get a free quote</h2>

            <p class="free-quote__atf__panel__copy">Take the next step to never having to mow the lawn again. Enter your first name and email address below to get your free, no obligation quote.</p>

            @include('partials.forms.free-quote')
        </div>
    </div>
</section>

<section class="free-quote__details">
    <div class="container">
        <div class="free-quote__location">
            <h2 class="free-quote__details__heading">Can I see a demo?</h2>

            <div class="free-quote__details__text">If you'd like to see Automower<sup>&reg;</sup> in action for yourself, <a href="mailto:per@emblarobotics.com?subject=Schedule a demo">send Per Lindberg an email</a> to schedule your free, personal demo at Embla Robotics HQ (see address below).</div>

            <div class="free-quote__location__map">
                <div class="map-wrapper">
                    <iframe width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=embla%20robotics&key=AIzaSyAvLpEmzwkiHacg4CbG7oKmAF9uDCMJ9Gw" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        <div class="free-quote__contact">
            <h2 class="free-quote__details__heading">Who can I contact for more information?</h2>

            <div class="contact__info">
                <img class="contact__info__image" src="@asset('images/per_lindberg.jpg')" />

                <div class="contact__info__text">
                    <div class="contact__info__name">Per Lindberg</div>
                    <div class="contact__info__title">Founder, Embla Robotics</div>

                    <div class="contact__info__email">
                        <i class="material-icons">email</i>
                        <a href="mailto:per@emblarobotics.com">per@emblarobotics.com</a>
                    </div>

                    <div class="contact__info__phone">
                        <i class="material-icons">phone</i>
                        <a href="tel:+18012164686">(801) 216-4686</a>
                    </div>
                </div>
            </div>

            <div class="contact__divider"></div>

            <div class="contact__company">
                <img src="@asset('images/embla_robotics_contact_logo.svg')" />
                <div class="contact__company__slogan">Your Utah Automower<sup>&reg;</sup> Specialist</div>
            </div>
        </div>
    </div>
</section>
