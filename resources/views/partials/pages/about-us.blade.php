<section class="about-us">
    <div class="about-us__embla-robotics">
        <div class="about-us__image"
            style="background-image: url(@asset('images/the_future_of_lawn_care.jpg'));">
        </div>

        <div class="about-us__text">
            <div>
                <h2 class="about-us__heading"><strong>Embla Robotics</strong> - Your Utah Automower<sup>&reg;</sup> Specialist</h2>

                <p>People across the world are replacing their conventional lawn mowers for robotic mowers, freeing themselves from the chore of mowing and giving them a consistently great looking lawn.</p>
                
                <p>Embla Robotics was created to provide smart, environmentally friendly lawn care products for homeowners. Per Lindberg is the founder of the company with strong links to Sweden and has a passion for smart homeowner solutions.</p>

                <p>Embla Robotics is an authorized Husqvarna dealer offering consultation, installation and maintenance of Automower<sup>&reg;</sup>, Husqvarna's state-of-the-art robotic lawn mower.</p>

                <div class="about-us__contact">
                    <div class="about-us__contact__location">
                        <a href="https://www.google.com/maps/place/Embla+Robotics/@40.430013,-111.8074117,17z/data=!3m1!4b1!4m5!3m4!1s0x874d8088917bef21:0xd4e08fd2fe1448!8m2!3d40.430013!4d-111.805223" 
                            target="_blank"
                            class="about-us__contact__item">
                            <i class="material-icons">location_on</i>
                            <span>6137 W 10930 N<br />Highland, UT 84003</span>
                        </a>
                    </div>

                    <div class="about-us__contact__email-phone">
                        <div class="about-us__contact__item">
                            <i class="material-icons">email</i>
                            <a href="mailto:info@emblarobotics.com">info@emblarobotics.com</a>
                        </div>

                        <div class="about-us__contact__item">
                            <i class="material-icons">phone</i>
                            <a href="tel:+18012164686">(801) 216-4686</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-us__husqvarna">
        <div class="about-us__image"
            style="background-image: url(@asset('images/epic_logger.jpg'));">
        </div>

        <div class="about-us__text">
            <div>
                <h2 class="about-us__heading"><strong>Husqvarna</strong> - over 325 years of innovation</h2>

                <p>Husqvarna Group is the world's largest producer of outdoor power products including robotic lawn mowers, garden tractors, chainsaws and trimmers. The Group is also the European leader in consumer watering products and one of the world leaders in cutting equipment and diamond tools for the construction and stone industries.</p>

                <p>Founded in Sweden in 1689, Husqvarna is celebrating over 325 years of engineering innovation. The Group's products and solutions are sold via dealers and retailers to both consumers and professional users in more than 100 countries.</p>

                <p>The Husqvarna Automower<sup>&reg;</sup> is a robotic lawn mower that was first developed by Husqvarna more than 20 years ago. Today, Automower<sup>&reg;</sup> is a market leader in robotic mowers, and since 1995 more than 1 million Husqvarna robotic mowers have been sold worldwide.</p>
            </div>
        </div>
    </div>
</section>
