<section class="how-automower-works__intro" 
    style="background-image: url(@asset('images/grass_bg.jpg'));">
    <div class="container">
        <h1 class="how-automower-works__intro__heading">How Automower<sup>&reg;</sup> Works</h1>

        <div class="how-automower-works__intro__video">
            <div class="video-wrapper">
                <iframe width="1280" height="720" src="https://www.youtube.com/embed/KE7O3dK07nQ?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>

            <a class="how-automower-works__intro__video__link" href="https://www.youtube.com/watch?v=LmdZHwtk71Q&list=PL0VcazyXHqEqXo1HAFT-I4UHXIf9WjO4R&index=3" target="_blank">
                <i class="material-icons">ondemand_video</i>
                <span>Watch more Automower<sup>&reg;</sup> videos</span>
            </a>
        </div>
    </div>
</section>

<section class="how-automower-works__overview"
    style="background-image: url(@asset('images/430_closeup.jpg'));">
    <div class="how-automower-works__overview__cover-filter">
        <div class="container">
            <div class="how-automower-works__features">
                <div class="how-automower-works__features-item">
                    <img class="how-automower-works__features-item__icon" src="@asset('images/icons/benefits/advanced_navigation.svg')" />
                    <h3 class="how-automower-works__features-item__heading">Advanced Navigation</h3>
                    <div class="how-automower-works__features-item__text">Several navigation methods, including GPS (on 430X and 450X), ensure Automower<sup>&reg;</sup> mows your entire lawn, even if your yard is complex.</div>
                </div>

                <div class="how-automower-works__features-item">
                    <img class="how-automower-works__features-item__icon" src="@asset('images/icons/benefits/low_noise.svg')" />
                    <h3 class="how-automower-works__features-item__heading">No Noise. No Fuss.</h3>
                    <div class="how-automower-works__features-item__text">You’ll be surprised how quickly you forget about your Automower<sup>&reg;</sup>. You’ll hardly notice it as it navigates your yard doing its job quickly, quietly and efficiently.</div>
                </div>

                <div class="how-automower-works__features-item">
                    <img class="how-automower-works__features-item__icon" src="@asset('images/icons/benefits/manages_steep_slopes.svg')" />
                    <h3 class="how-automower-works__features-item__heading">Manages Steep Slopes</h3>
                    <div class="how-automower-works__features-item__text">Automower<sup>&reg;</sup> can cope with slopes up to 45% (24°), thanks to large rear wheels, optimized design and smart behavior.</div>
                </div>

                <div class="how-automower-works__features-item">
                    <img class="how-automower-works__features-item__icon" src="@asset('images/icons/benefits/no_track_marks.svg')" />
                    <h3 class="how-automower-works__features-item__heading">No Track Marks</h3>
                    <div class="how-automower-works__features-item__text">Automower<sup>&reg;</sup> will automatically vary the route back to the charger in order to avoid track marks. Mowing in a seemingly random pattern gives you a carpet-like lawn throughout your garden.</div>
                </div>

                <div class="how-automower-works__features-item">
                    <img class="how-automower-works__features-item__icon" src="@asset('images/icons/benefits/automower_connect.svg')" />
                    <h3 class="how-automower-works__features-item__heading">Automower Connect</h3>
                    <div class="how-automower-works__features-item__text">Control the operation of your Automower<sup>&reg;</sup> from your iOS or Android smartphone, including locating Automower<sup>&reg;</sup> in case of theft.</div>
                </div>

                <div class="how-automower-works__features-item">
                    <img class="how-automower-works__features-item__icon" src="@asset('images/icons/benefits/alarm.svg')" />
                    <h3 class="how-automower-works__features-item__heading">Alarm with PIN Code</h3>
                    <div class="how-automower-works__features-item__text">Whenever Automower<sup>&reg;</sup> is lifted, a PIN code is required to prevent the alarm from being activated. A stolen Automower<sup>&reg;</sup> is useless as it can’t be matched to other charging stations.</div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('partials.testimonials')
