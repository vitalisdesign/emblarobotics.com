@extends('layouts.app')

@section('content')
    @while(have_posts()) @php(the_post())
        @include('partials.pages.how-automower-works')
    @endwhile
@endsection
