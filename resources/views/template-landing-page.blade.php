<?php
/* Template Name: Landing Page */
global $post;
?>

@extends('layouts.landing')

@section('content')
    @while(have_posts()) @php(the_post())
        @include('partials.pages.landing.' . $post->post_name)
    @endwhile
@endsection
