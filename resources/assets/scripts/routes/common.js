/*global ajax, dataLayer, players */
import 'material-design-lite';
import 'jquery.easing';
import 'slick-carousel';

export default {
    init() {
        toggleHeaderNav();
        hashScroll();
        freeQuoteForm();
        testimonialsSlider();
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};

/**
 * Toggle header nav.
 */
function toggleHeaderNav() {
    jQuery('.header__nav__toggle').click(function() {
        jQuery('.header__nav').toggleClass('header__nav--open');
    });

    jQuery(document).on('click', function(event) {
        if (!jQuery(event.target).closest('.header__nav__toggle').length) {
            jQuery('.header__nav').removeClass('header__nav--open');
        }
    });
}

/**
 * Scroll to page sections when hash links are clicked.
 */
function hashScroll() {
    jQuery('.hash-scroll').click(function() {
        var target = jQuery(this).attr('href');
        var offset = jQuery(target).offset().top - jQuery('.header').outerHeight() + 5;
        jQuery('.header__nav').removeClass('header__nav--open');

        jQuery('html, body').animate({
            scrollTop: offset,
        }, 500, 'easeInOutCubic');

        return false;
    });
}

/**
 * Submit free quote form via AJAX.
 */
function freeQuoteForm() {
    jQuery('.free-quote__form').submit(function() {
        var form = jQuery(this);
        var data = form.serializeArray();
        data.push({
            'name': 'action',
            'value': 'process_free_quote_form',
        });

        jQuery.ajax({
            type: 'POST',
            url: ajax.url,
            data: jQuery.param(data),
            dataType: 'json',
            beforeSend: function() {
                jQuery(form).find('button[type=submit]').attr('disabled', true);
            },
            success: function(response) {
                if (typeof dataLayer !== 'undefined') {
                  dataLayer.push({'event': 'requested_free_quote'})
                }

                jQuery(form).find('.user-feedback').html(response.message).addClass('success').removeClass('error').fadeIn();
            },
            error: function(response) {
                jQuery(form).find('.user-feedback').html(response.responseJSON.message).addClass('error').removeClass('success').fadeIn();
                jQuery(form).find('button[type=submit]').removeAttr('disabled');
            },
        });

        return false;
    });
}

/**
 * Initialize testimonials slider.
 */
function testimonialsSlider() {
    jQuery('.testimonials__slider').slick({
        arrows: false,
        fade: true,
    }).on('beforeChange', function(event, slick, currentSlide) {
        pauseYoutubeVideo(currentSlide);
    });

    jQuery('.testimonials__pager__item').click(function() {
        let index = jQuery(this).data('rel');
        jQuery('.testimonials__slider').slick('slickGoTo', index);

        jQuery('.testimonials__pager__item').removeClass('active');
        jQuery(this).addClass('active');

        if (jQuery('.testimonials__slider').slick('slickCurrentSlide') == index) {
            pauseYoutubeVideo(index);
            jQuery('#slide-' + index).removeClass('playing');
        }

        jQuery('.testimonials').removeClass('active');
    });

    jQuery('.testimonials__slide__play').click(function() {
        let index = jQuery(this).data('rel');

        jQuery('#slide-' + index).addClass('playing');
        playYoutubeVideo(index);

        jQuery('.testimonials').addClass('active');
    });

    function playYoutubeVideo(index) {
        if (typeof players[index].playVideo === 'function') {
            players[index].playVideo();
        }
    }

    function pauseYoutubeVideo(index) {
        if (typeof players[index].pauseVideo === 'function') {
            players[index].pauseVideo();
        }
    }
}
