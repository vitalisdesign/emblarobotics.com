<?php

namespace App;

use Illuminate\Contracts\Container\Container as ContainerContract;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Config;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('google/material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', false, null);

    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_localize_script('sage/main.js', 'ajax', ['url' => admin_url('admin-ajax.php')]);
}, 100);

/**
 * Register custom post types
 */
add_action('init', function() {
    register_post_type('product',
        [
            'labels' => [
                'name' => __('Products'),
                'add_new_item' => __('Add New Product'),
                'search_items' => __('Search Products'),
                'not_found' => __('No products found.'),
                'singular_name' => __('Product')
            ],
            'public' => true,
            'hierarchical' => true,
            'menu_icon' => 'dashicons-cart',
            'rewrite' => ['slug' => 'products'],
            'supports' => ['title', 'thumbnail']
        ]
    );

    register_post_type('download',
        [
            'labels' => [
                'name' => __('Downloads'),
                'add_new_item' => __('Add New Download'),
                'search_items' => __('Search downloads'),
                'not_found' => __('No downloads found.'),
                'singular_name' => __('Download')
                ],
            'public' => true,
            'hierarchical' => true,
            'menu_icon' => 'dashicons-download',
            'rewrite' => ['slug' => 'downloads'],
            'supports' => ['title', 'thumbnail']
        ]
    );

    register_post_type('testimonial',
        [
            'labels' => [
                'name' => __('Testimonials'),
                'add_new_item' => __('Add New Testimonial'),
                'search_items' => __('Search testimonials'),
                'not_found' => __('No testimonials found.'),
                'singular_name' => __('Testimonial')
            ],
        'public' => true,
        'hierarchical' => true,
        'menu_icon' => 'dashicons-format-quote',
        'rewrite' => ['slug' => 'testimonials'],
        'supports' => ['title']
        ]
    );
});

/**
 * Mark parent navigation active when on a custom post type single page
 */
add_action('nav_menu_css_class', function($classes, $item) {
    global $post;

    $current_post_type = get_post_type_object(get_post_type($post->ID));
    $current_post_type_slug = $current_post_type->rewrite['slug'];

    $menu_slug = strtolower(trim($item->url));

    if (strpos($menu_slug,$current_post_type_slug) !== false) {
       $classes[] = 'current-menu-item';
    }

    return $classes;
}, 10, 2);

/**
 * Add ACF options page.
 */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => 'Options',
        'position' => 30,
    ]);
}

 /**
  * Add ® to "How Automower Works" nav menu item
  */
add_filter('wp_nav_menu_items', function($items, $args) {
    $items = str_replace('Automower', 'Automower<sup>&reg;</sup>', $items);
    return $items;
}, 10, 2);

/**
 * Google Analytics and Facebook Conversion Tracking
 */
add_action('wp_head', function() {
    ?>
    <meta property="og:image" content="<?= asset_path('images/the_future_of_lawn_care.jpg'); ?>" />
    <?php
    if (WP_ENV == 'production' && ! is_user_logged_in()) {
        ?>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WK9FTRG');</script>
        <!-- End Google Tag Manager -->
        <?php
    }
});

/**
 * Insert code after opening body tag.
 */
add_action('after_body_open_tag', function() {
    if (WP_ENV == 'production' && ! is_user_logged_in()) {
        ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WK9FTRG"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <?php
    }
});

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));

    // hide the frontend admin bar
    show_admin_bar(false);
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Sage config
     */
    $paths = [
        'dir.stylesheet' => get_stylesheet_directory(),
        'dir.template'   => get_template_directory(),
        'dir.upload'     => wp_upload_dir()['basedir'],
        'uri.stylesheet' => get_stylesheet_directory_uri(),
        'uri.template'   => get_template_directory_uri(),
    ];
    $viewPaths = collect(preg_replace('%[\/]?(resources/views)?[\/.]*?$%', '', [STYLESHEETPATH, TEMPLATEPATH]))
        ->flatMap(function ($path) {
            return ["{$path}/resources/views", $path];
        })->unique()->toArray();

    config([
        'assets.manifest' => "{$paths['dir.stylesheet']}/../dist/assets.json",
        'assets.uri'      => "{$paths['uri.stylesheet']}/dist",
        'view.compiled'   => "{$paths['dir.upload']}/cache/compiled",
        'view.namespaces' => ['App' => WP_CONTENT_DIR],
        'view.paths'      => $viewPaths,
    ] + $paths);

    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (ContainerContract $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view'], $app);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= App\\asset_path({$asset}); ?>";
    });
});

/**
 * Init config
 */
sage()->bindIf('config', Config::class, true);
