<?php
/**
 * Process free quote submission.
 */
function process_free_quote_form() {
    $email = $_POST['email'];
    $first_name = sanitize_text_field($_POST['first_name']);
    $error_message = '';

    if (empty($email)) {
        $error_message = 'Please enter your email address';
    } else if (! is_email($email)) {
        $error_message = 'That email address is not valid';
    }

    if (empty($first_name)) {
        $error_message = 'Please enter your first name';
    }

    if (empty($error_message)) {
        $email = sanitize_email($email);

        // send quote request to subscriber
        $subscriber_to = $first_name . ' <' . $email . '>';
        $subscriber_subject = 'Here\'s your free Automower quote';
        $subscriber_headers = [
            'From: Per Lindberg <per@emblarobotics.com',
            'Content-Type: text/html; charset=UTF-8'
        ];

        ob_start(); ?>

        Hi <?= $first_name; ?>,<br /><br />

        Thank you for contacting us regarding the Automower - the robotic lawnmower from Husqvarna.<br /><br />

        Our company, Embla Robotics, sells and installs these incredible robots in Utah, and we offer free onsite consultation to help you get the best Automower installation for your lawn.<br /><br />

        There are several Automower models to choose from depending on the size and complexity of your lawn:<br /><br />

        <table>
        <tr>
            <td>Automower 310, covers up to 0.25 acre</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$1,299.95</td>
        </tr>
        <tr>
            <td>Automower 115H, covers up to 0.4 acre</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$1,599.95</td>
        </tr>
        <tr>
            <td>Automower 315/X, covers up to 0.4 acre</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$1,499.95 - $1,999.95</td>
        </tr>
        <tr>
            <td>Automower 430X/XH, covers up to 0.8 acre</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$2,699.95</td>
        </tr>
        <tr>
            <td>Automower 450X/XH, covers up to 1.25 acre</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$3,499.95</td>
        </tr>
        </table><br />

        Please see our <a href="https://emblarobotics.com" target="_blank">website</a> for more information on the Automower and the models we offer.<br />
        <a href="https://emblarobotics.com/wp-content/uploads/2016/07/Automower-Brochure-2019.pdf" target="_blank">Here</a> is the link to our brochure.<br /><br />

        We provide professional installation with a cable layer machine that completely hides and protects the boundary & guide wires. The installation costs between $500-$900 depending on the size and complexity of your lawn. In addition, you will need an installation kit ($99-$250, depending on the size of your lawn).<br /><br />

        Please note that we also offer up to <strong>48 months interest free financing</strong> via Sheffield and Synchrony.<br /><br />

        You are more than welcome to stop by my property in Highland to see the Automower in action for yourself. I've had it running for three seasons now and it works incredibly well. See my address below:<br /><br />

        <a href="https://www.google.com/maps/dir//6137+W+10930+N,+Highland,+UT+84003" target="_blank">
        6137 W 10930 N<br />
        Highland, UT 84003<br />
        </a>
        Phone: <a href="tel:8012164686">(801) 216-4686</a><br /><br />

        Click <a href="https://www.youtube.com/watch?v=LIhWvMFgDgs" target="_blank">here</a> to see a video of our Automower in action.<br /><br />

        We can also run a free test setup at your property to see how the Automower would work in your environment. Could you tell me a bit about your lawn? What is the size and complexity?<br /><br />

        Looking forward to your feedback.<br /><br />

        Regards,<br />
        Per Lindberg<br />
        Owner<br /><br />

        <strong>Embla Robotics, LLC</strong><br />
        6137 W 10930 N<br />
        Highland, UT 84003<br />
        Email: <a href="mailto:per@emblarobotics.com">per@emblarobotics.com</a><br />
        Office: <a href="tel:8012164686">(801) 216-4686</a><br />
        Cell: <a href="tel:8016002024">(801) 600-2024</a><br />
        <a href="https://emblarobotics.com" target="_blank">www.emblarobotics.com</a><br /><br />

        <em>Embla Robotics is an Authorized Husqvarna Automower® Dealer providing consulting, installation, maintenance and winter storage.</em>

        <?php $subscriber_message = ob_get_clean();
        wp_mail($subscriber_to, $subscriber_subject, $subscriber_message, $subscriber_headers);

        // send notification email to admin
        $admin_to = 'Per Lindberg <per@emblarobotics.com>';
        $admin_subject = 'You have a new request from Embla Robotics';
        $admin_headers = [
            'From: Embla Robotics <wordpress@emblarobotics.com',
            'Content-Type: text/html; charset=UTF-8'
        ];

        ob_start(); ?>

        You have a new request from Embla Robotics:<br /><br />

        Email address: <a href="mailto:<?= $email; ?>"><?= $email; ?></a>

        <?php $admin_message = ob_get_clean();
        wp_mail($admin_to, $admin_subject, $admin_message, $admin_headers);

        // HubSpot integration
        $hubspot = SevenShores\Hubspot\Factory::create(HUBSPOT_API_KEY);
        try {
            // create HubSpot contact
            $contact = $hubspot->contacts()->create([
                [
                    'property' => 'email',
                    'value' => $email,
                ],
                [
                    'property' => 'firstname',
                    'value' => $first_name,
                ],
                [
                    'property' => 'lifecyclestage',
                    'value' => 'lead',
                ],
                [
                    'property' => 'hs_lead_status',
                    'value' => 'NEW',
                ],
            ]);

            // log quote request email
            $engagement = [
                'type' => 'EMAIL',
            ];
            $associations = [
                'contactIds' => [$contact['vid']],
            ];
            $metadata = [
                'from' => [
                    'email' => 'per@emblarobotics.com',
                    'firstName' => 'Per',
                    'lastName' => 'Lindberg',
                ],
                'to' => [
                    'email' => $subscriber_to,
                ],
                'subject' => $subscriber_subject,
                'html' => $subscriber_message,
            ];
            $hubspot->engagements()->create($engagement, $associations, $metadata);
        } catch (exception $e) {
            error_log(print_r($e, true));
        }

        $response = [
            'message' => 'Thanks, we\'ll be in touch with your quote soon!'
        ];
    } else {
        http_response_code(422);
        $response['message'] = $error_message;
    }

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_process_free_quote_form', 'process_free_quote_form');
add_action('wp_ajax_nopriv_process_free_quote_form', 'process_free_quote_form');
